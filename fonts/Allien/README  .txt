
Licenses :
https://creativemarket.com/licenses

------------------------------------------------


Allien is a display sans serif font duo with modern and fun type font. 
Perfect for displays, headers, invitations, save the dates, weddings, and so much more, Allien will become a playful staple in your font library!


~ Revnede


------------------------------------------------

Support

Mail support : yehezkielazizal@gmai.com 

Dribbble : dribbble.com/yehezkielazizal

