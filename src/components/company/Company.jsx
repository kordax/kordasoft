import React from 'react';
import "./Company.css"
import * as localeRU from "../../locales/ru";
import * as localeEN from "../../locales/en";
import {MainContext} from "../Context";

export default function Company() {
    return (
        <MainContext.Consumer>
            {(context) => (
                <div className="main-body">
                    <div className="main-body-image">
                        <div className="main-body-image-content">
                            <div className="main-body-image-text">
                                {
                                    (() => {
                                        if (context.state.language === 'ru') {
                                            return localeRU.company.image_text
                                        } else {
                                            return localeEN.company.image_text
                                        }
                                    })()
                                }
                            </div>
                        </div>
                    </div>
                    <div className="main-body-content-wrapper">
                        <div className="main-body-how">
                            <div className="company-logo-wrapper"/>
                        </div>
                        <div className="main-body-content">
                            <div className="company-text">
                                {
                                    (() => {
                                        if (context.state.language === 'ru') {
                                            return localeRU.company.foundation
                                        } else {
                                            return localeEN.company.foundation
                                        }
                                    })()
                                }
                                <h2 align="center">
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.why
                                            } else {
                                                return localeEN.home.pros.why
                                            }
                                        })()
                                    }
                                </h2>
                                {
                                    (() => {
                                        if (context.state.language === 'ru') {
                                            return localeRU.company.pros
                                        } else {
                                            return localeEN.company.pros
                                        }
                                    })()
                                }
                                <h2 align="center">
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.company.location.header
                                            } else {
                                                return localeEN.company.location.header
                                            }
                                        })()
                                    }
                                </h2>
                                {
                                    (() => {
                                        if (context.state.language === 'ru') {
                                            return localeRU.company.location.body
                                        } else {
                                            return localeEN.company.location.body
                                        }
                                    })()
                                }
                            </div>
                            <div className="company-images-wrapper">
                                <div className="company-petersburg-image-wrapper">
                                    <img alt="Kordasoft" className="company-petersburg-image" src={require("./petersburg_01.jpg")}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </MainContext.Consumer>
    );
}