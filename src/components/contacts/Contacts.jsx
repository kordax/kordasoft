import React, { Component } from 'react';
import { MainContext } from "../Context";
import {Button, ButtonGroup, Form, Input} from 'reactstrap'
import "./Contacts.css"
import * as localeRU from "../../locales/ru";
import * as localeEN from "../../locales/en";
import * as emailjs from 'emailjs-com'
import {withRouter} from "react-router-dom";

const ServiceTypes = {
    UNDEFINED: 'undefined',
    IMPLEMENTATION: 'implementation',
    QUALITY_ASSURANCE: 'quality assurance',
    TECHNICAL_SUPPORT: 'technical support',
    OUTSOURCING: 'outsourcing'
};

const SendButtonStatus = {
    UNDEFINED: 0,
    STANDBY: 1,
    SENDING: 2
};

class Contacts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            service: ServiceTypes.UNDEFINED,
            text: '',
            message_html: '',
            email: '',
            name: '',
            company: '',
            position: '',
            phone: '',
            website: '',
            budget: '',
            sendButtonStatus: SendButtonStatus.STANDBY,
            sendButtonColor: '#AAAAAA',
            sendButtonDisabled: true
        };

        this.handleServiceType = this.handleServiceType.bind(this);
        this.submitEmail = this.submitEmail.bind(this);
        this.onLoadRecaptcha = this.onLoadRecaptcha.bind(this);
        this.verifyCallback = this.verifyCallback.bind(this);
        this.sendButtonSendingStatus = this.sendButtonSendingStatus.bind(this)
        this.sendButtonStatus = this.sendButtonStatus.bind(this)
    }

    componentDidMount() {
        if (this.captchaDemo) {
            this.captchaDemo.reset();
        }
    }

    onLoadRecaptcha() {
        if (this.captchaDemo) {
            this.captchaDemo.reset();
        }
    }

    verifyCallback(recaptchaToken) {

    }

    handleServiceType(e, serviceType) {
        e.preventDefault();
        this.setState({service_type: serviceType, sendButtonDisabled: false, sendButtonColor: ''})
    }

    handleChange = (param, e) => {
        e.preventDefault();
        this.setState({ [param]: e.target.value })
    };

    sendButtonSendingStatus(context) {
        if (context.state.language === 'ru') {
            return localeRU.contacts.ask_form.send_button_sending
        } else {
            return localeEN.contacts.ask_form.send_button_sending
        }
    }

    sendButtonStatus(context) {
        if (this.state.sendButtonStatus === SendButtonStatus.SENDING) {
            if (context.state.language === 'ru') {
                return localeRU.contacts.ask_form.send_button_sending
            } else {
                return localeEN.contacts.ask_form.send_button_sending
            }
        } else {
            if (context.state.language === 'ru') {
                return localeRU.contacts.ask_form.send_button_value
            } else {
                return localeEN.contacts.ask_form.send_button_value
            }
        }
    }

    submitEmail(e) {
        e.preventDefault();

        this.setState({sendButtonStatus: SendButtonStatus.SENDING});
        this.setState({sendButtonColor: '#AAAAAA'});
        this.setState({sendButtonDisabled: true});

        let mail = {
            from_name: 'no-reply@kordasoft.com',
            to_name: 'mail@kordasoft.com',
            subject: 'Contact Form message',
            message_html: this.state.text,
            email: this.state.email,
            name: this.state.name,
            company: this.state.company,
            position: this.state.position,
            phone: this.state.phone,
            website: this.state.website,
            service: this.state.service_type,
            budget: this.state.budget,
        };

        console.log("Sending email: " + mail);
        const responseStatusPromise = emailjs.send(
            'smtp_server',
            'template_y3Z6jnPx',
            mail,
            'user_qcvUAUANvRKmVmEKCw993'
        );

        responseStatusPromise.then(response => {
            if (response.status === 200) {
                console.info("Your request was sent");
                this.setState({sendButtonStatus: 'Message was sent'});
            } else {
                console.error("Failed to send request, please try later");
                this.setState({sendButtonStatus: 'Error'});
            }
        });

        responseStatusPromise.catch(e => {
            console.error("Failed to deliver email: " + e);
            this.setState({sendButtonStatus: 'Error'});
        });

        this.reset();
    }

    reset() {
        this.setState({
            service: ServiceTypes.UNDEFINED,
            text: '',
            message_html: '',
            email: '',
            name: '',
            company: '',
            position: '',
            phone: '',
            website: '',
            budget: 0,
            sendButtonColor: '',
            sendButtonDisabled: false
        })
    }

    render() {
        return (
            <MainContext.Consumer>
                {(context) => (
                    <div className="main-body">
                        <div className="main-body-image">
                            <div className="main-body-image-content">
                                <div className="main-body-image-text">
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.contacts.image_text
                                            } else {
                                                return localeEN.contacts.image_text
                                            }
                                        })()
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="main-body-content-wrapper">
                            <div className="main-body-how">
                                {
                                    (() => {
                                        if (context.state.language === 'ru') {
                                            return localeRU.contacts.ask_form.title
                                        } else {
                                            return localeEN.contacts.ask_form.title
                                        }
                                    })()
                                }
                            </div>
                            <div className="main-body-content">
                                <Form onSubmit={(e) => this.submitEmail(e, context)}>
                                    <div className="ask-form-wrapper">
                                        <div className="ask-form-content">
                                            <div className="ask-form-entry-wrapper ask-form-entry-wrapper-compact">
                                                <div className="ask-form-entry-title">
                                                    <div className="ask-form-entry-title-text">
                                                        {
                                                            (() => {
                                                                if (context.state.language === 'ru') {
                                                                    return localeRU.contacts.ask_form.service_type.title
                                                                } else {
                                                                    return localeEN.contacts.ask_form.service_type.title
                                                                }
                                                            })()
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ask-form-entry-choice">
                                                        <Button className="ask-form-button" onClick={(e) => this.handleServiceType(e, ServiceTypes.IMPLEMENTATION)} active={this.state.service_type === ServiceTypes.IMPLEMENTATION}>
                                                            {
                                                                (() => {
                                                                    if (context.state.language === 'ru') {
                                                                        return localeRU.contacts.ask_form.service_type.development
                                                                    } else {
                                                                        return localeEN.contacts.ask_form.service_type.development
                                                                    }
                                                                })()
                                                            }
                                                        </Button>
                                                        <Button className="ask-form-button" onClick={(e) => this.handleServiceType(e, ServiceTypes.QUALITY_ASSURANCE)} active={this.state.service_type === ServiceTypes.QUALITY_ASSURANCE}>
                                                            {
                                                                (() => {
                                                                    if (context.state.language === 'ru') {
                                                                        return localeRU.contacts.ask_form.service_type.qa
                                                                    } else {
                                                                        return localeEN.contacts.ask_form.service_type.qa
                                                                    }
                                                                })()
                                                            }
                                                        </Button>
                                                        <Button className="ask-form-button" onClick={(e) => this.handleServiceType(e, ServiceTypes.TECHNICAL_SUPPORT)} active={this.state.service_type === ServiceTypes.TECHNICAL_SUPPORT}>
                                                            {
                                                                (() => {
                                                                    if (context.state.language === 'ru') {
                                                                        return localeRU.contacts.ask_form.service_type.support
                                                                    } else {
                                                                        return localeEN.contacts.ask_form.service_type.support
                                                                    }
                                                                })()
                                                            }
                                                        </Button>
                                                </div>
                                            </div>
                                            <div className="ask-form-entry-wrapper">
                                                <div className="ask-form-entry-title">
                                                    <div className="ask-form-entry-title-text">
                                                        {
                                                            (() => {
                                                                if (context.state.language === 'ru') {
                                                                    return localeRU.contacts.ask_form.budget.title
                                                                } else {
                                                                    return localeEN.contacts.ask_form.budget.title
                                                                }
                                                            })()
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ask-form-entry-choice">
                                                    <Input type="number" min={(() => {
                                                            if (context.state.language === 'ru') {
                                                                return 300000
                                                            } else {
                                                                return 5000
                                                            }
                                                        })()
                                                    } className="ask-form-input" placeholder={
                                                        (() => {
                                                            if (context.state.language === 'ru') {
                                                                return "RUB (Рубли)"
                                                            } else {
                                                                return "USD"
                                                            }
                                                        })()
                                                    }
                                                           required
                                                           name="budget"
                                                           value={this.state.budget}
                                                           onChange={this.handleChange.bind(this, 'budget')}
                                                    />
                                                </div>
                                            </div>
                                            <div className="ask-form-entry-wrapper">
                                                <div className="ask-form-entry-title">
                                                    <div className="ask-form-entry-title-text">
                                                        {
                                                            (() => {
                                                                if (context.state.language === 'ru') {
                                                                    return localeRU.contacts.ask_form.info.title
                                                                } else {
                                                                    return localeEN.contacts.ask_form.info.title
                                                                }
                                                            })()
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ask-form-entry-choice">
                                                    <Input className="ask-form-text-area"
                                                           required
                                                           col="100"
                                                           rows="120"
                                                           placeholder="Please provide additional information..."
                                                           name="text"
                                                           value={this.state.text}
                                                           onChange={this.handleChange.bind(this, 'text')}
                                                    />
                                                </div>
                                            </div>
                                            <div className="ask-form-entry-wrapper ask-form-entry-wrapper-compact">
                                                <div className="ask-form-entry-title">
                                                    <div className="ask-form-entry-title-text">
                                                        {
                                                            (() => {
                                                                if (context.state.language === 'ru') {
                                                                    return localeRU.contacts.ask_form.name
                                                                } else {
                                                                    return localeEN.contacts.ask_form.name
                                                                }
                                                            })()
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ask-form-entry-choice">
                                                    <Input className="ask-form-input"
                                                           required
                                                           name="name"
                                                           value={this.state.name}
                                                           onChange={this.handleChange.bind(this, 'name')}/>
                                                </div>
                                            </div>
                                            <div className="ask-form-entry-wrapper ask-form-entry-wrapper-compact">
                                                <div className="ask-form-entry-title">
                                                    <div className="ask-form-entry-title-text">
                                                        {
                                                            (() => {
                                                                if (context.state.language === 'ru') {
                                                                    return localeRU.contacts.ask_form.email
                                                                } else {
                                                                    return localeEN.contacts.ask_form.email
                                                                }
                                                            })()
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ask-form-entry-choice">
                                                    <Input className="ask-form-input"
                                                           required
                                                           type="email"
                                                           name="email"
                                                           value={this.state.email}
                                                           onChange={this.handleChange.bind(this, 'email')}/>
                                                </div>
                                            </div>
                                            <div className="ask-form-entry-wrapper ask-form-entry-wrapper-compact">
                                                <div className="ask-form-entry-title">
                                                    <div className="ask-form-entry-title-text">
                                                        {
                                                            (() => {
                                                                if (context.state.language === 'ru') {
                                                                    return localeRU.contacts.ask_form.company
                                                                } else {
                                                                    return localeEN.contacts.ask_form.company
                                                                }
                                                            })()
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ask-form-entry-choice">
                                                    <Input className="ask-form-input"
                                                           required
                                                           name="company"
                                                           value={this.state.company}
                                                           onChange={this.handleChange.bind(this, 'company')}/>
                                                </div>
                                            </div>
                                            <div className="ask-form-entry-wrapper ask-form-entry-wrapper-compact">
                                                <div className="ask-form-entry-title">
                                                    <div className="ask-form-entry-title-text">
                                                        {
                                                            (() => {
                                                                if (context.state.language === 'ru') {
                                                                    return localeRU.contacts.ask_form.position
                                                                } else {
                                                                    return localeEN.contacts.ask_form.position
                                                                }
                                                            })()
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ask-form-entry-choice">
                                                    <Input className="ask-form-input"
                                                           required
                                                           name="position"
                                                           value={this.state.position}
                                                           onChange={this.handleChange.bind(this, 'position')}/>
                                                </div>
                                            </div>
                                            <div className="ask-form-entry-wrapper ask-form-entry-wrapper-compact">
                                                <div className="ask-form-entry-title">
                                                    <div className="ask-form-entry-title-text">
                                                        {
                                                            (() => {
                                                                if (context.state.language === 'ru') {
                                                                    return localeRU.contacts.ask_form.phone
                                                                } else {
                                                                    return localeEN.contacts.ask_form.phone
                                                                }
                                                            })()
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ask-form-entry-choice">
                                                    <Input className="ask-form-input"
                                                           required
                                                           name="phone"
                                                           value={this.state.phone}
                                                           onChange={this.handleChange.bind(this, 'phone')}/>
                                                </div>
                                            </div>
                                            <div className="ask-form-entry-wrapper ask-form-entry-wrapper-compact">
                                                <div className="ask-form-entry-title">
                                                    <div className="ask-form-entry-title-text">
                                                        {
                                                            (() => {
                                                                if (context.state.language === 'ru') {
                                                                    return localeRU.contacts.ask_form.website
                                                                } else {
                                                                    return localeEN.contacts.ask_form.website
                                                                }
                                                            })()
                                                        }
                                                    </div>
                                                </div>
                                                <div className="ask-form-entry-choice">
                                                    <Input className="ask-form-input"
                                                           required
                                                           type="website"
                                                           name="website"
                                                           value={this.state.website}
                                                           onChange={this.handleChange.bind(this, 'website')}/>
                                                </div>
                                            </div>
                                            <div className="send-button-wrapper">
                                                <input disabled={this.state.sendButtonDisabled} type="submit" style={{backgroundColor: this.state.sendButtonColor}} value={this.sendButtonStatus(context)} className="send-button"/>
                                            </div>
                                        </div>
                                    </div>
                                    {/*<div className="main-body-bottom-filler"/>*/}
                                </Form>
                            </div>
                        </div>
                    </div>
                )}
            </MainContext.Consumer>
        );
    };
}

export default withRouter(Contacts)