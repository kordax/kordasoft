// import React, { Component } from 'react';
// import {MainContext} from "../Context";
// import "./AskForm.css"
// import * as localeRU from "../../locales/ru";
// import * as localeEN from "../../locales/en";
//
// export default class AskForm extends Component {
//     render() {
//         return (
//             <MainContext.Consumer>
//                 {(context) => (
//                     <div>
//                         <div className="ask-form-overlay"/>
//                         <div className="ask-form-wrapper">
//                             <div className="ask-form-header">
//                                 <button className="ask-form-exit-button"/>
//                             </div>
//                             <div className="ask-form-content">
//                                 <div className="ask-form-entry-wrapper">
//                                     <div className="ask-form-entry-title">
//                                         <div className="ask-form-entry-title-text">
//                                             Service type
//                                         </div>
//                                     </div>
//                                     <div className="ask-form-entry-choice">
//                                         <button className="ask-form-button">Design & Implementation</button>
//                                         <button className="ask-form-button">Quality Assurance</button>
//                                         <button className="ask-form-button">Technical Support</button>
//                                     </div>
//                                 </div>
//                                 <div className="ask-form-right-filler"/>
//                                 <div className="ask-form-entry-wrapper">
//                                     <div className="ask-form-entry-title">
//                                         <div className="ask-form-entry-title-text">
//                                             Approximate budget
//                                         </div>
//                                     </div>
//                                     <div className="ask-form-entry-choice">
//                                         <input type="number" className="ask-form-input"/>
//                                         {
//                                             (() => {
//                                                 if (context.state.language === 'ru') {
//                                                     return "(RUB)"
//                                                 } else {
//                                                     return "(USD)"
//                                                 }
//                                             })()
//                                         }
//                                     </div>
//                                 </div>
//                                 <div className="ask-form-right-filler"/>
//                                 <div className="ask-form-entry-wrapper">
//                                     <div className="ask-form-entry-title">
//                                         <div className="ask-form-entry-title-text">
//                                             Additional information
//                                         </div>
//                                     </div>
//                                     <div className="ask-form-entry-choice">
//                                         <textarea className="ask-form-text-area" placeholder="Please provide additional information..."/>
//                                     </div>
//                                 </div>
//                                 <div className="ask-form-right-filler"/>
//                             </div>
//                         </div>
//                     </div>
//                 )}
//             </MainContext.Consumer>
//         );
//     };
// }