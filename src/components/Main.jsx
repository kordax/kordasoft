import React from 'react';
import {Switch, Route, withRouter} from 'react-router-dom';

import Home from './home/Home';
import Company from './company/Company';
import Contacts from './contacts/Contacts';
import Error_NotFound from "./error/Error_NotFound";

import { MainContext } from './Context'

export default function Main() {
    return (
        <MainContext.Consumer>
            {(context) => (
                <Switch>
                    <Route exact path='/' component={withRouter(Home)} context={context}/>
                    <Route exact path='/contacts' component={withRouter(Contacts)} context={context}/>
                    <Route exact path='/company' component={withRouter(Company)} context={context}/>
                    <Route exact path='/404' component={Error_NotFound} context={context} status={404}/>
                </Switch>
            )}
        </MainContext.Consumer>
    );
}