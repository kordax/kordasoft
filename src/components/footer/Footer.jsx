import React, { Component } from 'react';
import "./Footer.css"
import {MainContext} from "../Context";
import * as localeRU from "../../locales/ru";
import * as localeEN from "../../locales/en";
import AskForm from "../ask_form/AskForm";

export default class Footer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            form_active: false
        };

        this.highlight = this.highlight.bind(this);
    }

    highlight(event) {
        event.preventDefault();
        this.setState({form_active: true})
    }

    render() {
        return (
            <MainContext.Consumer>
                {(context) => (
                    <div className="footer">
                        {
                            (() => {
                                if (this.state.form_active) {
                                    return <AskForm/>
                                }
                            })()
                        }
                        <div className="footer-container">
                            <div className="footer-contacts-container">
                                <a className="footer-contacts-email" href="mailto:mail@kordasoft.com">mail@kordasoft.com</a>
                                <a className="footer-contacts-ask-button" href="/contacts">
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.footer.contacts.ask_question
                                            } else {
                                                return localeEN.footer.contacts.ask_question
                                            }
                                        })()
                                    }
                                </a>
                            </div>
                        </div>
                        <div className="footer-copyright">
                            {
                                (() => {
                                    if (context.state.language === 'ru') {
                                        return localeRU.footer.copyright
                                    } else {
                                        return localeEN.footer.copyright
                                    }
                                })()
                            }
                        </div>
                    </div>
                )}
            </MainContext.Consumer>
        );
    }
}