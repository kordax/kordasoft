import React from 'react';
import "./Services.css"

export default function Services() {
    return (
        // TODO: Language
        <div className="main-body">
            <div className="main-body-image">
                <div className="main-body-image-content">
                    <div className="main-body-image-text">
                        Services
                    </div>
                </div>
            </div>
            <div className="main-body-content-wrapper">
                <div className="main-body-content">
                    <div className="services-content-wrapper">
                        <div className="services-content-entry-wrapper">
                            <div className="services-content-entry">
                                <div className="services-content-entry-header">
                                    Software development
                                </div>
                                <div className="services-content-entry-body">
                                    We will develop an application to suit your business needs.<br/>
                                    No need to hire development team:<br/>we will do everything for you.<br/>
                                    Robust and complex backend systems with top-notch technology?<br/>It's not a problem anymore.
                                </div>
                            </div>
                            <div className="services-content-filler-right">
                                {/*{`🡄`}*/}
                            </div>
                        </div>
                        <div className="services-content-entry-wrapper">
                            <div className="services-content-filler-left"/>
                            <div className="services-content-entry">
                                <div className="services-content-entry-header">
                                    Support
                                </div>
                                <div className="services-content-entry-body">
                                    In Kordasoft we believe that customers are not less important than anything else.<br/>
                                    Our team can provide support for your existing projects and your customers.<br/>
                                    We're happy to help you with your current problems and make your customers happy.
                                </div>
                            </div>
                        </div>
                        <div className="services-content-entry-wrapper">
                            <div className="services-content-entry">
                                <div className="services-content-entry-header">
                                    Quality Assurance
                                </div>
                                <div className="services-content-entry-body">
                                    Your project is finished but you want some things to be polished,
                                    to make your customers experience even better?<br/>
                                    We offer quality assurance services with experienced QA engineers and developers who
                                    can to automate your QA processes and reduce your release and update times.
                                </div>
                            </div>
                            <div className="services-content-filler-right"/>
                        </div>
                        <div className="services-content-entry-wrapper">
                            <div className="services-content-filler-left"/>
                            <div className="services-content-entry">
                                <div className="services-content-entry-header">
                                    Architecture design
                                </div>
                                <div className="services-content-entry-body">
                                    You're not sure about best practices or some technology that your team is willing to use?<br/>
                                    We will help you with our software architecture.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}