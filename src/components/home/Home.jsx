import React, { Component } from 'react';
import './Home.css';
import * as localeRU from "../../locales/ru";
import * as localeEN from "../../locales/en";
import {MainContext} from "../Context";
import ProsSlider from "../slider/ProsSlider";

export default class Home extends Component {
   render () {
        return (
            <MainContext.Consumer>
                {(context) => (
                    <div className="main-body">
                        <div className="main-body-image">
                            <div className="main-body-image-content">
                                <div className="main-body-image-text">
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.image_text
                                            } else {
                                                return localeEN.home.image_text
                                            }
                                        })()
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="main-body-content-wrapper">
                            <div className="main-body-how">
                                {
                                    (() => {
                                        if (context.state.language === 'ru') {
                                            return localeRU.home.how
                                        } else {
                                            return localeEN.home.how
                                        }
                                    })()
                                }
                            </div>
                            <div className="main-body-content">
                                <div className="main-body-how-steps">
                                    <div className="main-body-how-steps-vert-entry">
                                        <div className="main-body-how-steps-vert-header">
                                            {
                                                (() => {
                                                    if (context.state.language === 'ru') {
                                                        return localeRU.home.steps.design.header
                                                    } else {
                                                        return localeEN.home.steps.design.header
                                                    }
                                                })()
                                            }
                                        </div>
                                        <div className="main-body-how-steps-vert-content-wrapper">
                                            <div className="main-entry-content-image-wrapper">
                                                <img alt="architecture" className="main-entry-content-image" src={require("./img/arch_notxt.png")}/>
                                            </div>
                                            <div className="main-body-how-steps-vert-content-text">
                                                {
                                                    (() => {
                                                        if (context.state.language === 'ru') {
                                                            return localeRU.home.steps.design.body
                                                        } else {
                                                            return localeEN.home.steps.design.body
                                                        }
                                                    })()
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="main-body-how-steps-vert-entry">
                                        <div className="main-body-how-steps-vert-header">
                                            {
                                                (() => {
                                                    if (context.state.language === 'ru') {
                                                        return localeRU.home.steps.implementation.header
                                                    } else {
                                                        return localeEN.home.steps.implementation.header
                                                    }
                                                })()
                                            }
                                        </div>
                                        <div className="main-body-how-steps-vert-content-wrapper">
                                            <div className="main-entry-content-image-wrapper">
                                                <img alt="coding" className="main-entry-content-image" src={require("./img/coding_notxt.png")}/>
                                            </div>
                                            <div className="main-body-how-steps-vert-content-text">
                                                {
                                                    (() => {
                                                        if (context.state.language === 'ru') {
                                                            return localeRU.home.steps.implementation.body
                                                        } else {
                                                            return localeEN.home.steps.implementation.body
                                                        }
                                                    })()
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="main-body-how-steps-vert-entry">
                                        <div className="main-body-how-steps-vert-header">
                                            {
                                                (() => {
                                                    if (context.state.language === 'ru') {
                                                        return localeRU.home.steps.qa.header
                                                    } else {
                                                        return localeEN.home.steps.qa.header
                                                    }
                                                })()
                                            }
                                        </div>
                                        <div className="main-body-how-steps-vert-content-wrapper">
                                            <div className="main-entry-content-image-wrapper">
                                                <img alt="qa" className="main-entry-content-image" src={require("./img/qa.png")}/>
                                            </div>
                                            <div className="main-body-how-steps-vert-content-text">
                                                {
                                                    (() => {
                                                        if (context.state.language === 'ru') {
                                                            return localeRU.home.steps.qa.body
                                                        } else {
                                                            return localeEN.home.steps.qa.body
                                                        }
                                                    })()
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="main-body-how-steps-vert-entry">
                                        <div className="main-body-how-steps-vert-header">
                                            {
                                                (() => {
                                                    if (context.state.language === 'ru') {
                                                        return localeRU.home.steps.support.header
                                                    } else {
                                                        return localeEN.home.steps.support.header
                                                    }
                                                })()
                                            }
                                        </div>
                                        <div className="main-body-how-steps-vert-content-wrapper">
                                            <div className="main-entry-content-image-wrapper">
                                                <img alt="support" className="main-entry-content-image" src={require("./img/support.png")}/>
                                            </div>
                                            <div className="main-body-how-steps-vert-content-text">
                                                {
                                                    (() => {
                                                        if (context.state.language === 'ru') {
                                                            return localeRU.home.steps.support.body
                                                        } else {
                                                            return localeEN.home.steps.support.body
                                                        }
                                                    })()
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="main-body-bottom-filler"/>
                            <div className="main-body-why-us">
                                <div className="main-body-why-us-header">
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.why
                                            } else {
                                                return localeEN.home.pros.why
                                            }
                                        })()
                                    }
                                </div>
                                <ProsSlider/>
                            </div>
                        </div>
                    </div>
                )}
            </MainContext.Consumer>
        );
    }
}