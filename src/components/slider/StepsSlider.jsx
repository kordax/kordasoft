import React, { Component } from "react";
import Slider from "react-slick";
import "./Slider.css"
import DesignStep from "./steps/DesignStep";
import ImplementationStep from "./steps/ImplementationStep";
import QAStep from "./steps/QAStep";

export default class StepsSlider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currIndex: 1,
            steps: new Map()
        };

        this.state.steps.set(1, "Design");
        this.state.steps.set(2, "Implementation");
        this.state.steps.set(3, "Quality Assurance");

        this.nextStep = this.nextStep.bind(this);
        this.prevStep = this.prevStep.bind(this);
    }

    nextStep () {
        const lastIndex = this.state.steps.size;
        const { currIndex } = this.state;
        const shouldResetIndex = currIndex === lastIndex;
        const index = shouldResetIndex ? 1 : currIndex + 1;

        this.setState({
            currIndex: index
        });
    }

    prevStep () {
        const lastIndex = this.state.steps.size;
        const { currIndex } = this.state;
        const shouldResetIndex = currIndex === 1;
        const index = shouldResetIndex ? lastIndex : currIndex - 1;

        this.setState({
            currIndex: index
        });
    }

    render() {
        var settings = {
            dots: false,
            infinite: true,
            // variableWidth: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            centerMode: false,
        };

        return (
            <div className="main-body-how-step-slider">
                {/*<Arrow direction="left" callback={this.prevStep} glyph="arrow" id="id-slider-arrow-left"/>*/}
                <Slider {...settings}>
                    <DesignStep index={1}/>
                    <ImplementationStep index={2}/>
                    <QAStep index={3}/>
                    {/*<Step index={3} body={this.state.steps.get(3)}/>*/}
                </Slider>
                {/*<Arrow direction="right" callback={this.nextStep} glyph="arrow" id="id-slider-arrow-right"/>*/}
            </div>
        );
    }
}