import React, { Component } from "react";
import "../Slider.css"

export default class Arrow extends Component {
    render () {
        return (
            <div className="slider-arrow-wrapper">
                <button className={`slider-arrow-button ${this.props.direction}`} onClick={this.props.callback}/>
            </div>
        );
    }
};