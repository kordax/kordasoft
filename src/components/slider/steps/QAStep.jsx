import React from "react";
import Step from "./Step";
import {MainContext} from "../../Context";
import * as localeRU from "../../../locales/ru";
import * as localeEN from "../../../locales/en";

export default class QAStep extends Step {
    render() {
        return (
            <MainContext.Consumer>
                {(context) => (
                    <div className="main-body-how-step-slide">
                        <div className="main-body-how-step-header">
                            {
                                (() => {
                                    if (context.state.language === 'ru') {
                                        return localeRU.home.slider.steps.qa.header
                                    } else {
                                        return localeEN.home.slider.steps.qa.header
                                    }
                                })()
                            }
                        </div>
                        <div className="main-body-how-step-body">
                            {
                                (() => {
                                    if (context.state.language === 'ru') {
                                        return localeRU.home.slider.steps.qa.body
                                    } else {
                                        return localeEN.home.slider.steps.qa.body
                                    }
                                })()
                            }
                        </div>
                    </div>
                )}
            </MainContext.Consumer>
        );
    }
}