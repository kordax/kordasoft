import React from "react";

export const ProsStep = ({children}) => (
    <div className="main-body-why-us-entry-wrapper">
        {children}
    </div>
);