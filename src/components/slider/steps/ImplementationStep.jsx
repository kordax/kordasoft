import React from "react";
import Step from "./Step";
import * as localeRU from "../../../locales/ru";
import * as localeEN from "../../../locales/en";
import {MainContext} from "../../Context";

export default class ImplementationStep extends Step {
    render() {
        return (
            <MainContext.Consumer>
                {(context) => (
                    <div className="main-body-how-step-slide">
                        <div className="main-body-how-step-header">
                            {
                                (() => {
                                    if (context.state.language === 'ru') {
                                        return localeRU.home.slider.steps.implementation.header
                                    } else {
                                        return localeEN.home.slider.steps.implementation.header
                                    }
                                })()
                            }
                        </div>
                        <div className="main-body-how-step-body">
                            {
                                (() => {
                                    if (context.state.language === 'ru') {
                                        return localeRU.home.slider.steps.implementation.body
                                    } else {
                                        return localeEN.home.slider.steps.implementation.body
                                    }
                                })()
                            }
                        </div>
                    </div>
                )}
            </MainContext.Consumer>
        );
    }
}