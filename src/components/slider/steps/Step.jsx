import React, { Component } from "react";

export default class Step extends Component {
    render() {
        return (
            <div className="main-body-how-step-slide">
                <div className="main-body-how-step-index">
                    {this.props.index}
                </div>
                <div className="main-body-how-step-header">
                    {this.props.header}
                </div>
                <div className="main-body-how-step-body">
                    {this.props.body}
                </div>
            </div>
        );
    }
}