import React, { Component } from "react";
import Slider from "react-slick";
import "./Slider.css"
import {ProsStep} from "./steps/ProsStep";
import * as localeRU from "../../locales/ru";
import * as localeEN from "../../locales/en";
import {MainContext} from "../Context";

export default class ProsSlider extends Component {
    render() {
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            centerMode: false,
        };

        return (
            <MainContext.Consumer>
                {(context) => (
                    <div className="main-body-why-us-slider">
                        <Slider {...settings}>
                            <ProsStep>
                                <div className="main-body-why-us-entry-image-wrapper">
                                    <img className="main-body-why-us-entry-image" src={require("./img/pros_design.png")} alt="design"/>
                                </div>
                                <div className="main-body-why-us-entry-text">
                                    <h3>{
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.experience.header
                                            } else {
                                                return localeEN.home.pros.experience.header
                                            }
                                        })()
                                    }</h3><br/>
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.experience.body
                                            } else {
                                                return localeEN.home.pros.experience.body
                                            }
                                        })()
                                    }
                                </div>
                            </ProsStep>
                            <ProsStep>
                                <div className="main-body-why-us-entry-image-wrapper">
                                    <img className="main-body-why-us-entry-image" src={require("./img/pros_services.png")} alt="we care"/>
                                </div>
                                <div className="main-body-why-us-entry-text">
                                    <h3>{
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.we_care.header
                                            } else {
                                                return localeEN.home.pros.we_care.header
                                            }
                                        })()
                                    }</h3>
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.we_care.body
                                            } else {
                                                return localeEN.home.pros.we_care.body
                                            }
                                        })()
                                    }
                                </div>
                            </ProsStep>
                            <ProsStep>
                                <div className="main-body-why-us-entry-image-wrapper">
                                    <img className="main-body-why-us-entry-image" src={require("./img/pros_testing.png")} alt="testing"/>
                                </div>
                                <div className="main-body-why-us-entry-text">
                                    <h3>{
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.we_test.header
                                            } else {
                                                return localeEN.home.pros.we_test.header
                                            }
                                        })()
                                    }</h3>
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.we_test.body
                                            } else {
                                                return localeEN.home.pros.we_test.body
                                            }
                                        })()
                                    }
                                </div>
                            </ProsStep>
                            <ProsStep>
                                <div className="main-body-why-us-entry-image-wrapper">
                                    <img className="main-body-why-us-entry-image" src={require("./img/pros_flexibility.png")} alt="flexibility"/>
                                </div>
                                <div className="main-body-why-us-entry-text">
                                    <h3>{
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.flexibility.header
                                            } else {
                                                return localeEN.home.pros.flexibility.header
                                            }
                                        })()
                                    }</h3>
                                    {
                                        (() => {
                                            if (context.state.language === 'ru') {
                                                return localeRU.home.pros.flexibility.body
                                            } else {
                                                return localeEN.home.pros.flexibility.body
                                            }
                                        })()
                                    }
                                </div>
                            </ProsStep>
                        </Slider>
                    </div>
                )}
            </MainContext.Consumer>
        );
    }
}