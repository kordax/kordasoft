import React from 'react';
import "./Error.css"
import {MainContext} from "../Context";
import * as localeRU from "../../locales/ru";
import * as localeEN from "../../locales/en";

export default function Error_WIP() {
    return (
        <MainContext.Consumer>
            {(context) => (
                <div className="main-body">
                    <div className="main-body-content-wrapper">
                        <div className="main-body-content">
                            <div className="error-text">
                                <img className="error-image" alt="wip" src={require("./img/wip_01.png")}/><br/>
                                {
                                    (() => {
                                        if (context.state.language === 'ru') {
                                            return localeRU.error.wip
                                        } else {
                                            return localeEN.error.wip
                                        }
                                    })()
                                }
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </MainContext.Consumer>
    );
}