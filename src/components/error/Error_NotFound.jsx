import React from 'react';
import "./Error.css"
import * as localeRU from "../../locales/ru";
import * as localeEN from "../../locales/en";
import {MainContext} from "../Context";

export default function Error_NotFound() {
    return (
        <MainContext.Consumer>
            {(context) => (
                <div className="main-body">
                    <div className="main-body-content-wrapper">
                        <div className="main-body-content">
                            <div className="error-text">
                                <h1>Error: 404</h1>
                                {
                                    (() => {
                                        if (context.state.language === 'ru') {
                                            return localeRU.error.not_found
                                        } else {
                                            return localeEN.error.not_found
                                        }
                                    })()
                                }<br/>
                                :'(
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </MainContext.Consumer>
    );
}