import React from 'react';
import '../Header.css';

export default function Logo() {
    return (
        <div className="hdr-logo">
            <a className="hdr-logo" href="/" title="Home">
                <img alt="Kordasoft" src={require("./logo_278x36.png")}/>
                {/*<svg className="hdr-logo">*/}
                {/*    <use xlinkHref={`${HeaderLogo}#HeaderLogo`}/>*/}
                {/*</svg>*/}
            </a>
        </div>
    );
}