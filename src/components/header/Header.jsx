import React from 'react';
import './Header.css';
import Inventory from './inventory/Inventory'
import LangDropdown from "./langdropdown/LangDropdown";
import Logo from "./logo/Logo";
import {MainContext} from "../Context";

export default function Header() {
    return (
        <MainContext.Consumer>
            {(context) => (
                <header className="main-header">
                    <div className="hdr-outer">
                        <Logo context={context}/>
                        <Inventory context={context}/>
                        <LangDropdown context={context}/>
                    </div>
                </header>
            )}
        </MainContext.Consumer>
    );
}