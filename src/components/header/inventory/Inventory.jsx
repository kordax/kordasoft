import React from 'react';
import './Inventory.css';
import * as localeRU from './../../../locales/ru'
import * as localeEN from './../../../locales/en'

import {MainContext} from "../../Context";

export default function Inventory() {
    return (
        <MainContext.Consumer>
            {(context) => (
                <div className="hdr-inventory">
                    <nav className="hdr-inventory-nav">
                        <a className="hdr-inv-button" href="/company/" title="Company">
                            {
                                (() => {
                                    if (context.state.language === 'ru') {
                                        return localeRU.header.inventory.company
                                    } else {
                                        return localeEN.header.inventory.company
                                    }
                                })()
                            }
                        </a>
                        {/*<a className="hdr-inv-button" href="/services/" title="Services">*/}
                        {/*    {*/}
                        {/*        (() => {*/}
                        {/*            if (context.state.language === 'ru') {*/}
                        {/*                return localeRU.header.inventory.services*/}
                        {/*            } else {*/}
                        {/*                return localeEN.header.inventory.services*/}
                        {/*            }*/}
                        {/*        })()*/}
                        {/*    }*/}
                        {/*</a>*/}
                        <a className="hdr-inv-button" href="/contacts/" title="Contacts">
                            {
                                (() => {
                                    if (context.state.language === 'ru') {
                                        return localeRU.header.inventory.contacts
                                    } else {
                                        return localeEN.header.inventory.contacts
                                    }
                                })()
                            }
                        </a>
                    </nav>
                </div>
            )}
        </MainContext.Consumer>
    );
}