import React, { Component } from 'react';
import '../Header.css';
import {MainContext} from "../../Context";
import * as localeRU from "../../../locales/ru";
import * as localeEN from "../../../locales/en";
import {withRouter} from 'react-router-dom';

class LangDropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showMenu: false,
            color: "#333333",
            prevColor: "#333333"
        };

        this.highlight = this.highlight.bind(this);
        this.revertHighlight = this.revertHighlight.bind(this);
        this.switchDropdown = this.switchDropdown.bind(this);
        this.closeDropdown = this.closeDropdown.bind(this);
        this.handleLang = this.handleLang.bind(this);
        this.handleSpecificPrefixCase = this.handleSpecificPrefixCase.bind(this);
        this.handleSpecificWWWPrefixCase = this.handleSpecificWWWPrefixCase.bind(this);
    }

    prefixes() {
        return ["en", "ru"];
    }

    handleLang(location, prefix) {
        let result = '';
        if (window.location.host.includes("www.")) {
            if (window.location.host.includes("www." + prefix + ".")) {
                result = "http://" + window.location.host + location.pathname
            } else {
                result = this.handleSpecificWWWPrefixCase(prefix, location);
            }
        } else {
            if (window.location.host.includes(prefix + ".")) {
                result = "http://" + window.location.host + location.pathname
            } else {
                if (prefix !== "en") {
                    result = "http://" + prefix + "." + window.location.host + location.pathname;
                } else {
                    result = this.handleSpecificPrefixCase(prefix, location);
                }
            }
        }

        return result;
    }

    handleSpecificPrefixCase(prefix, location) {
        let hostname = window.location.host;
        this.prefixes().forEach(pr => {
                hostname = hostname.replace(pr + ".", "");
            }
        );

        return "http://" + hostname + location.pathname;
    }

    handleSpecificWWWPrefixCase(prefix, location) {
        let hostname = window.location.host;
        this.prefixes().forEach(pr => {
                hostname = hostname.replace(pr + ".", "");
            }
        );

        if (prefix !== "en") {
            return "http://" + hostname.replace("www.", "www." + prefix + ".") + location.pathname;
        } else {
            return "http://" + hostname + location.pathname;
        }

    }

    render() {
        return (
            <MainContext.Consumer>
                {(context) => (
                    <div className="hdr-lang-menu">
                        <button className="hdr-lang-menu-selector" style={{backgroundColor: this.state.color}}
                                onClick={this.switchDropdown}
                                onMouseOver={this.highlight}
                                onMouseLeave={this.revertHighlight}>
                            {
                                (() => {
                                    if (context.state.language === 'ru') {
                                        return localeRU.header.language_selector
                                    } else {
                                        return localeEN.header.language_selector
                                    }
                                })()
                            }
                        </button>
                        {
                            this.state.showMenu ? (
                                <div className="hdr-lang-dropdown">
                                    <a href={this.handleLang(this.props.location, "en")} className="hdr-lang-menu-button" data-language="en">
                                        {
                                            (() => {
                                                return localeEN.header.language
                                            })()
                                        }
                                    </a>
                                    <a href={this.handleLang(this.props.location, "ru")} className="hdr-lang-menu-button" data-language="ru">
                                        {
                                            (() => {
                                                return localeRU.header.language
                                            })()
                                        }
                                    </a>
                                </div>
                            ) : null
                        }
                    </div>
                )}
            </MainContext.Consumer>
        );
    }

    highlight(event) {
        event.preventDefault();
        this.setState({
            color: "#222b2d",
        });
    }

    revertHighlight() {
        this.setState({
            color: this.state.prevColor
        });
    }

    switchDropdown(event) {
        event.preventDefault();
        this.setState({
            color: "#ff8200ff",
            prevColor: "#ff8200ff"
        });

        this.setState({ showMenu: true }, () => {
            document.addEventListener('click', this.closeDropdown);
        });
    }

    closeDropdown() {
        this.setState({
            color: "#333333",
            prevColor: "#333333"
        });

        this.setState({ showMenu: false }, () => {
            document.removeEventListener('click', this.closeDropdown);
        });
    }
}

export default withRouter(LangDropdown);