import React, { Component } from 'react'

export const MainContext = React.createContext();

export class MainProvider extends Component {
    constructor(props) {
        super(props);

        this.changeLanguage = this.changeLanguage.bind(this);

        if (window.location.hostname.includes("ru.")) {
            this.state = {
                language: 'ru'
            };
        } else {
            this.state = {
                language: 'en'
            };
        }
    }

    changeLanguage = (event) => {
         console.log("Switching language to: " + event.target.dataset.language);
         this.setState({
            language: event.target.dataset.language
         })
    };

    render() {
        return (
            <MainContext.Provider value={{
                state: this.state,
                changeLanguage: this.changeLanguage
            }}>
                {this.props.children}
            </MainContext.Provider>
        )
    }
}