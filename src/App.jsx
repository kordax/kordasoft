import React from 'react';
import './App.css';
import Footer from "./components/footer/Footer";
import Header from "./components/header/Header";
import Main from "./components/Main";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {MainProvider} from "./components/Context";

// const {loadReCaptcha} = require("react-recaptcha-google");

function App() {
    // loadReCaptcha(); TODO: Enable if needed

    return (
      <MainProvider>
          <div className="main-div">
            <Header/>
            <Main/>
            <Footer/>
          </div>
      </MainProvider>
    );
}

export default App;
